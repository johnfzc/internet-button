# Internet Button

Because parents need internet control!

# Configuration

You need to create a .config file in the following format pointing to your OpenWrt router:

```
{"router": "192.168.1.1", "password": "dont_breed"}
```

For each device you want to control access for create a rule named `ib_User_Device`, for instance, 'ib_John_PC'


```
config rule
	option src 'lan'
	option name 'ib_John_PC'
	option src_mac '69:85:1C:F3:AF:12'
	option target 'REJECT'
	option enabled '0'
	option dest '*'
```

When you access the base of the application you'll see a page showing each user and their devices, as in the following example:

![SCreenie](https://z-c.ca/files/ib_example.png)