#!/usr/local/lib/ib/venv/bin/python

from typing import Optional
from fastapi import FastAPI, Form
from fastapi.responses import HTMLResponse, RedirectResponse
import threading
import json
import asyncssh
import re

lock = threading.Lock()

app = FastAPI()

config = None
victim_data = None

def load_config():
    with open ("/usr/local/lib/ib/.config", "r") as f:
        config_data = f.read()
        config = json.loads(config_data)
    return config


async def get_router_state():
    global config
    victim_data={}
    victim_rule=None

    if config is None:
        config = load_config()

    async with asyncssh.connect(config["router"], username="root", password=config["password"], 
        server_host_key_algs=['ssh-rsa'], known_hosts=None) as conn:
        result = await conn.run('uci show firewall | grep -E "name|enabled"')

    # Example output
    # firewall.@rule[12].name='ib_Aidan_phone'
    # firewall.@rule[12].enabled='0'
    # firewall.@rule[13].name='ib_Aidan_pc'
    # firewall.@rule[13].enabled='0'
    # firewall.@rule[15].name='ib_Riley_PC'
    # firewall.@rule[15].enabled='0'
    # firewall.@rule[16].enabled='0'

    for line in result.stdout.split("\n"):
        if "name" in line:
            m = re.search(r"rule\[(\d+)\].name='ib_(.*)_(.*)'", line)
            if m is not None:
                victim_rule=m.group(1)
                victim=m.group(2)
                device=m.group(3)
                if victim not in victim_data:
                    victim_data[victim]={}
                victim_data[victim][device]={
                    "enabled": False,
                    "rule": victim_rule
                }
            else:
                victim_rule=None

        if victim_rule != None and "enabled" in line:
            m = re.search(r"rule\[(\d+)\].enabled='(\d)'", line)
            if m is not None:
                rule=m.group(1)
                rule_enabled=m.group(2)
                if rule == victim_rule:
                    if rule_enabled == "0":
                        victim_data[victim][device]["enabled"]=True

    return victim_data


async def generate_html_response():
    html_content = """
    <html>
        <link rel="stylesheet" href="https://cdn.simplecss.org/simple.min.css">
        <head>
            <title>Internet Button</title>
        </head>
        <body>
    """

    with lock:
        status = await get_router_state()
    for user, devices in status.items():
        html_content+=f"<h1>{user}</h1><ul>"
        for device, state in devices.items():
            if state["enabled"] == True:
                pretty_state="Enabled"
                button="Disable"
            if state["enabled"] == False:
                pretty_state="Disabled"
                button="Enable"
            button=f"<form action=\"/victim/\" method=\"post\"><input type=hidden name=\"victim\" value=\"{user}_{device}\"><input type=\"submit\" value=\"{button}\"></form>"
            html_content+=f"<li>{device} - {pretty_state}{button}</li>"
        html_content+=f"</ul>"
    html_content+="</body></html>"

    return HTMLResponse(content=html_content, status_code=200)    

@app.get("/")
async def read_root():
    return await generate_html_response()

@app.post("/victim/")
async def victim(victim: str = Form(...)):
    with lock:
        status = await get_router_state()

    for user, devices in status.items():
        for device, state in devices.items():
            if victim == f"{user}_{device}":
                with lock:
                    if state["enabled"] == True:
                        command=f"uci delete firewall.@rule[{state['rule']}].enabled"
                    else:
                        command=f"uci set firewall.@rule[{state['rule']}].enabled='0'"
                    async with asyncssh.connect(config["router"], username="root", password=config["password"], 
                        server_host_key_algs=['ssh-rsa'], known_hosts=None) as conn:
                        result = await conn.run(f'{command}; uci commit; /etc/init.d/firewall restart')
                        print (result.stdout)
                        print (result.stderr)

    return RedirectResponse("/",status_code=302)
