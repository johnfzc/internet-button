FROM ubuntu:20.04 AS base

# Install dependencies
RUN apt update && apt --yes upgrade && apt --yes install python3 && apt --yes autoclean

# Create a standard user to run the SSH CA
RUN useradd ib

# Create a new temporary image so we can compile/build a Python Virtual Environment
FROM base AS build

# Configure timezone settings
RUN TZ=UTC && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Install build dependencies
RUN apt --yes install python3-venv python3-dev build-essential python3-pip

# Create a new Python Virtual Environment and install required modules/libraries
RUN mkdir -p /usr/local/lib/ib/ 
RUN python3 -m venv /usr/local/lib/ib/venv 
RUN . /usr/local/lib/ib/venv/bin/activate && pip3 install --upgrade pip && pip3 install asyncssh fastapi uvicorn requests python-multipart

# Create the final image, consisting of the base image coupled with the Python Virtual Environment from the intermediate build image
FROM base AS prod

# Get the Python Virtual Environment from build
COPY --from=build /usr/local/lib/ib/ /usr/local/lib/ib/

COPY ./code/ /usr/local/lib/ib/code/

COPY .config /usr/local/lib/ib/

WORKDIR /usr/local/lib/ib/code/

CMD ["/usr/local/lib/ib/venv/bin/uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]

EXPOSE 80
